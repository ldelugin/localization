﻿using UnityEngine;
using UnityEditor;
using System.IO;

// TODO: Properly telling what each properties and fields are doing.

namespace Sparrow.Localization
{
	// System.IO: http://msdn.microsoft.com/en-us/library/system.io(v=vs.110).aspx

	/// <summary>
	/// File utility.
	/// </summary>
	public class FileUtility
	{
		public static void CreatFolder (string directory)
		{
			Directory.CreateDirectory(directory);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh(ImportAssetOptions.Default);
		}

		public static bool CheckFolder (string directory)
		{
			return Directory.Exists(directory);
		}

		public static void CheckAndCreateFolder (string directory)
		{
			if(!CheckFolder(directory))
			{
				CreatFolder(directory);
			}
		}

		public static string CombinePath (string path1, string path2)
		{
			return Path.Combine(path1, path2);
		}
	}
}