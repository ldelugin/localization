﻿using UnityEngine;
using UnityEditor;
using System.Xml.Serialization;
using System.IO;
using System.Globalization;

// TODO: Properly telling what each properties and fields are doing.
namespace Sparrow.Localization
{
	/// <summary>
	/// Localization utility.
	/// </summary>
	public class LocalizationUtility 
	{
		private static string databasePath = "Assets/Resources/Generated/";
		private static string databaseName = "LocalizationDatabase.asset";

		public static void CreateXML (object obj, string dataPath)
		{
			// Create a new empty namespace, this is needed to fix a bug, when trying to deserialize the xml on runtime.
			XmlSerializerNamespaces nameSpace = new XmlSerializerNamespaces();
			nameSpace.Add("", "");

			// Create a new serializer of the object wich will be passed in the parameter "obj".
			XmlSerializer serializer = new XmlSerializer(obj.GetType());

			// Create a new TextWriter, wich become a StreamWriter and use the dataPath as specifed path.
			TextWriter writer = new StreamWriter(dataPath);

			// Serialize the object and write the xml using the specifed stream.
			serializer.Serialize(writer, obj, nameSpace);

			// Close the writer. 
			writer.Close();

			// And refresh the AssetDatabase so the created xml object will be showed directly.
			AssetDatabase.Refresh(ImportAssetOptions.Default);
		}

		public static object LoadXML (object obj, string dataPath)
		{
			// Create a empty object, wich will be used as deserialized object and then be returned.
			object objectToReturn;

			// Create a new serializer object the object wich will be passed in the parameter "obj".
			XmlSerializer serializer = new XmlSerializer(obj.GetType());

			// Create a new TextReader, wich become a StreamReader and use the dataPath as specifedPath.
			TextReader reader = new StreamReader(dataPath);

			// The objectToReturn becomes the object wich will be Deserialized trough the StreamReader.
			objectToReturn = serializer.Deserialize(reader);

			// Close the reader.
			reader.Close();

			// Return the Deserialized object.
			return objectToReturn;
		}

		public static void LoadAllData ()
		{

		}

		public static void SaveAllData ()
		{
			// Saving everything in the database to xml.
			// Have to choose when to do this? Maybe only at button press of the user, but when the app is crashing everthing is gone.
			// Maybe at every change, how is the performance?
		}

		public static void CreateNewKey (LanguageKeys key)
		{
			LocalizationDatabase database = null;
			database = LocalizationUtility.LoadDatabase();

			database.AddNewKey(key);
		}

		public static void DeleteKey (LanguageKeys key)
		{
			LocalizationDatabase database = null;
			database = LocalizationUtility.LoadDatabase();

			//database.DeleteKey(key);
			//database.DeleteKeyFromAllLanguages(key);
		}

		public static void CreateNewLanguage (LanguageFile language)
		{
			LocalizationDatabase database = null;
			database = LocalizationUtility.LoadDatabase();

			database.AddNewLanguage(language.LanguageInfo.EnglishName);
		}

		public static void DeleteLanguage (LanguageFile language)
		{
			LocalizationDatabase database = null;
			database = LocalizationUtility.LoadDatabase();

			//database.DeleteLanguage(language);
		}

		[MenuItem("Sparrow/Localization/Create Database")]
		public static void CreateDatabase ()
		{
			// TODO: Add here a check, to check if the database allready is made, if so don't try to make it again. Use FileUtility.cs to check if the file allready exists.

			// Create a combined path for the Database.
			string path = Path.Combine(databasePath, databaseName);

			// Create the asset of type ScriptableObject wich will be creating and instance of the type LocalizationDatabase. Wich on his turn is derived from
			// the ScriptableObject class.
			ScriptableObject asset = ScriptableObject.CreateInstance<LocalizationDatabase>();

			// Check if the folder at "path" exists, if not create it.
			FileUtility.CheckAndCreateFolder(path);

			// Create the new asset at the "path"
			AssetDatabase.CreateAsset(asset, path);

			// Save all assets.
			AssetDatabase.SaveAssets();

			// Set the focus on the ProjectWindow
			EditorUtility.FocusProjectWindow();

			// And the asset will become the Selected Object.
			Selection.activeObject = asset;

			LocalizationUtility.PopulateCustomCulturesArray();
		}

		public static void PopulateCustomCulturesArray ()
		{
			CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
			CustomCultureInfo[] customCultures;

			LocalizationDatabase database = null;
			database = LoadDatabase();

			if(database != null)
			{
				customCultures = new CustomCultureInfo[cultures.Length];

				for(int i = 0; i < cultures.Length; i++)
				{
					customCultures[i] = new CustomCultureInfo();
					customCultures[i].DisplayName = cultures[i].DisplayName;
					customCultures[i].EnglishName = cultures[i].EnglishName;
					customCultures[i].Name = cultures[i].Name;
					customCultures[i].NativeName = cultures[i].NativeName;
					customCultures[i].LCID = cultures[i].LCID;
				}

				database.SetCustomCultures(customCultures);
			}
		}

		public static LocalizationDatabase LoadDatabase ()
		{
			string path = Path.Combine("Generated/", "LocalizationDatabase");
			return (LocalizationDatabase)Resources.Load(path);
		}

		[MenuItem("Sparrow/Localization/Test LoadDatabase")]
		public static void LoadDatabaseTest ()
		{
			LocalizationDatabase database = null;

			database = LoadDatabase();

			Debug.Log("Database: " + database);
		}

		[MenuItem("Sparrow/Localization/TestFunction CreateNewKey")]
		public static void TestFunctionCreateNewKey ()
		{
			LanguageKeys key = new LanguageKeys();
			key.Key = "Test.Key";
		
			CreateNewKey(key);
		}

		[MenuItem("Sparrow/Localization/TestFunction CreateNewLanguage")]
		public static void TestFunctionCreateNewLanguage ()
		{
			LanguageFile language = new LanguageFile();
			language.LanguageInfo = new CustomCultureInfo();
			language.LanguageInfo.EnglishName = "Dutch";

			CreateNewLanguage(language);
		}

		[MenuItem("Sparrow/Localization/TestFunction DeleteKey")]
		public static void TestFunctionDeleteKey ()
		{
			LanguageKeys key = new LanguageKeys();
			key.Key = "Test.Key";
			key.KeyValue = "ThisValue";

			DeleteKey(key);
		}

		[MenuItem("Sparrow/Localization/TestFunction DeleteLanguage")]
		public static void TestFunctionDeleteLanguage ()
		{
			LanguageFile language = new LanguageFile();
			language.LanguageInfo = new CustomCultureInfo();
			language.LanguageInfo.EnglishName = "Dutch";

			DeleteLanguage(language);
		}
	}
}