﻿using UnityEngine;
using System.Collections;

// TODO: Properly telling what each properties and fields are doing.

namespace Sparrow.Localization
{
	/// <summary>
	/// Root language file.
	/// </summary>
	[System.Serializable]
	public class RootLanguageFile 
	{
		/// <summary>
		/// The keys.
		/// </summary>
		[SerializeField]
		private LanguageKeys[] keys;

		/// <summary>
		/// All culture infos.
		/// </summary>
		[SerializeField]
		private CustomCultureInfo[] allCultureInfos;

		/// <summary>
		/// Gets or sets the keys.
		/// </summary>
		/// <value>The keys.</value>
		public LanguageKeys[] Keys
		{
			get { return keys; }
			set { keys = value; }
		}

		/// <summary>
		/// Gets or sets all culture infos.
		/// </summary>
		/// <value>All culture infos.</value>
		public CustomCultureInfo[] AllCultureInfos
		{
			get { return allCultureInfos; }
			set { allCultureInfos = value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Sparrow.Localization.RootLanguageFile"/> class.
		/// </summary>
		public RootLanguageFile ()
		{

		}
	}
}