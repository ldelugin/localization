﻿using UnityEngine;
using System.Collections;

// TODO: Properly telling what each properties and fields are doing.

namespace Sparrow.Localization
{
	/// <summary>
	/// Custom culture info.
	/// </summary>
	[System.Serializable]
	public class CustomCultureInfo 
	{
		/// <summary>
		/// The name.
		/// </summary>
		[SerializeField]
		private string name;

		/// <summary>
		/// The display name.
		/// </summary>
		[SerializeField]
		private string displayName;

		/// <summary>
		/// The name of the english.
		/// </summary>
		[SerializeField]
		private string englishName;

		/// <summary>
		/// The name of the native.
		/// </summary>
		[SerializeField]
		private string nativeName;

		/// <summary>
		/// The lcid.
		/// </summary>
		[SerializeField]
		private int lcid;

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		/// <summary>
		/// Gets or sets the display name.
		/// </summary>
		/// <value>The display name.</value>
		public string DisplayName
		{
			get { return displayName; }
			set { displayName = value; }
		}

		/// <summary>
		/// Gets or sets the name of the english.
		/// </summary>
		/// <value>The name of the english.</value>
		public string EnglishName
		{
			get { return englishName; }
			set { englishName = value; }
		}

		/// <summary>
		/// Gets or sets the name of the native.
		/// </summary>
		/// <value>The name of the native.</value>
		public string NativeName
		{
			get { return nativeName; }
			set { nativeName = value; }
		}

		/// <summary>
		/// Gets or sets the LCI.
		/// </summary>
		/// <value>The LCI.</value>
		public int LCID
		{
			get { return lcid; }
			set { lcid = value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Sparrow.Localization.CustomCultureInfo"/> class.
		/// </summary>
		public CustomCultureInfo ()
		{

		}
	}
}