﻿using UnityEngine;
using System.Collections.Generic;

namespace Sparrow.Localization
{
	public static class Extensions 
	{
		public static T[] RemoveAt<T> (this T[] original, int index)
		{
			T[] finalArray = new T[original.Length - 1];
			for(int i = 0; i < finalArray.Length; i++)
			{
				finalArray[i] = (i < index) ? original[i] : original[i + 1];
			}

			return finalArray;
		}

		public static T[] RemoveItemFromArray<T> (this T[] original, T itemToRemove)
		{
			T[] finalArray = new T[original.Length - 1];
			for(int i = 0; i < finalArray.Length; i++)
			{
				if(!original[i].Equals(itemToRemove))
				{
					finalArray[i] = original[i];
				}
			}

			return finalArray;
		}

		public static T[] AddItemToArray<T> (this T[] original, T itemToAdd)
		{
			T[] finalArray = new T[original.Length + 1];
			for(int i = 0; i < original.Length; i++)
			{
				Debug.Log("original item: " + original[i].ToString());
				finalArray[i] = original[i];
			}

			finalArray[finalArray.Length - 1] = itemToAdd;

			return finalArray;
		}
	}
}