﻿using UnityEngine;
using System.Collections;

// TODO: Properly telling what each properties and fields are doing.

namespace Sparrow.Localization
{
	/// <summary>
	/// Language keys.
	/// </summary>
	[System.Serializable]
	public class LanguageKeys 
	{
		/// <summary>
		/// The key.
		/// </summary>
		[SerializeField]
		private string key;

		/// <summary>
		/// The key value.
		/// </summary>
		[SerializeField]
		private string keyValue;

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		public string Key
		{
			get { return key; }
			set { key = value; }
		}

		/// <summary>
		/// Gets or sets the key value.
		/// </summary>
		/// <value>The key value.</value>
		public string KeyValue
		{
			get { return keyValue; }
			set { keyValue = value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Sparrow.Localization.LanguageKeys"/> class.
		/// </summary>
		public LanguageKeys ()
		{

		}
	}
}