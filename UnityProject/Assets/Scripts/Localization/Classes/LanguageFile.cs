﻿using UnityEngine;
using System.Collections;

// TODO: Properly telling what each properties and fields are doing.

namespace Sparrow.Localization
{
	/// <summary>
	/// Language file.
	/// </summary>
	[System.Serializable]
	public class LanguageFile 
	{
		/// <summary>
		/// The language info.
		/// </summary>
		[SerializeField]
		private CustomCultureInfo languageInfo;

		/// <summary>
		/// The keys.
		/// </summary>
		[SerializeField]
		private LanguageKeys[] keys;

		/// <summary>
		/// Gets or sets the language info.
		/// </summary>
		/// <value>The language info.</value>
		public CustomCultureInfo LanguageInfo
		{
			get { return languageInfo; }
			set { languageInfo = value; }
		}

		/// <summary>
		/// Gets or sets the keys.
		/// </summary>
		/// <value>The keys.</value>
		public LanguageKeys[] Keys
		{
			get { return keys; }
			set { keys = value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Sparrow.Localization.LanguageFile"/> class.
		/// </summary>
		public LanguageFile ()
		{

		}
	}
}