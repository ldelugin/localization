﻿using UnityEngine;
using System.Collections.Generic;

// TODO: Properly telling what each properties and fields are doing.

namespace Sparrow.Localization
{
	public class LocalizationDatabase : ScriptableObject
	{
		/// <summary>
		/// The root language file.
		/// </summary>
		[SerializeField]
		private RootLanguageFile rootLanguageFile;

		/// <summary>
		/// The language files.
		/// </summary>
		[SerializeField]
		private LanguageFile[] languageFiles;

		/// <summary>
		/// The instantiated.
		/// </summary>
		private bool instantiated;

		public bool IsInstantiated
		{
			get
			{
				if(PlayerPrefs.GetString("IsDatabaseInstantiated").Equals("false"))
				{
					instantiated = false;
				}

				else if(PlayerPrefs.GetString("IsDatabaseInstantiated").Equals("true"))
				{
					instantiated = true;
				}

				return instantiated;
			}
		}

		/// <summary>
		/// Raises the enable event.
		/// </summary>
		void OnEnable ()
		{
			if(!instantiated)
			{
				Initialisation();
			}
		}

		/// <summary>
		/// Initialisation this instance.
		/// </summary>
		void Initialisation ()
		{
			Debug.Log("@Database: Initialisation");

			rootLanguageFile = new RootLanguageFile();
			//rootLanguageFile.Keys = new LanguageKeys[0];
			//rootLanguageFile.AllCultureInfos = new CustomCultureInfo[0];

			//languageFiles = new LanguageFile[0];

			instantiated = true;
			PlayerPrefs.SetString("IsDatabaseInstantiated", "true");
		}

		void OnDestroy ()
		{
			Debug.Log("OnDestroy Database");
			PlayerPrefs.SetString("IsDatabaseInstantiated", "false");
		}

		public void SetCustomCultures (CustomCultureInfo[] customCultures)
		{
			rootLanguageFile.AllCultureInfos = customCultures;
		}

		public void AddNewKey (LanguageKeys key)
		{
			if(!CheckForKey(key))
			{
				Debug.Log("@AddNewKey - CheckForKey: " + key + " is false.");
				if(rootLanguageFile.Keys == null || rootLanguageFile.Keys.Length <= 0)
				{
					Debug.Log("@AddNewKey - rootLanguageFile.Keys is null, so create new array.");
					//rootLanguageFile.Keys = new LanguageKeys[0];
				}

				if(rootLanguageFile.Keys != null || rootLanguageFile.Keys.Length > 0)
				{
					Debug.Log("@AddNewKey - rootLanguageFile.Keys is not null, so add item to the keys array.");
					rootLanguageFile.Keys.AddItemToArray(key);
				}

				if(languageFiles != null || languageFiles.Length > 0)
				{
					Debug.Log("@AddNewKey - There are languages so we can add the key to the languages.");
					for(int i = 0; i < languageFiles.Length; i++)
					{
						Debug.Log("@AddNewKey - wich language: " + languageFiles[i].LanguageInfo.EnglishName);
						if(languageFiles[i].Keys == null || languageFiles[i].Keys.Length <= 0)
						{
							Debug.Log("@AddNewKey - " + languageFiles[i].LanguageInfo.EnglishName + " the array of the keys are null, so create new array.");
							languageFiles[i].Keys = new LanguageKeys[0];
						}

						if(languageFiles[i].Keys != null || languageFiles[i].Keys.Length > 0)
						{
							Debug.Log("@AddNewKey - " + languageFiles[i].LanguageInfo.EnglishName + " the array of the keys are not null, so check if the key is allready in this array.");
							if(!CheckForKey(languageFiles[i], key))
							{
								Debug.Log("@AddNewKey - key: " + key.Key + "The key is not in the array, so we can add it.");
								languageFiles[i].Keys.AddItemToArray(key);
							}
						}
					}
				}
			}
		}

		public void AddNewLanguage (string englishName)
		{
			Debug.Log("@AddNewLanguage - Check if the language we want to add, is a legit language.");
			for(int i = 0; i < rootLanguageFile.AllCultureInfos.Length; i++)
			{
				if(rootLanguageFile.AllCultureInfos[i].EnglishName.Equals(englishName))
				{
					Debug.Log("@AddNewLanguage - The " + englishName + " is legit so create a new language.");
					CreateNewLanguage(rootLanguageFile.AllCultureInfos[i]);
					break;
				}
			}
		}

		private void CreateNewLanguage (CustomCultureInfo customCulture)
		{
			// Create new language with all the proper info about that language.
			Debug.Log("@CreateNewLanguage - ");
			LanguageFile languageFile = new LanguageFile();
			languageFile.LanguageInfo = new CustomCultureInfo();
			languageFile.LanguageInfo.DisplayName = customCulture.DisplayName;
			languageFile.LanguageInfo.EnglishName = customCulture.EnglishName;
			languageFile.LanguageInfo.Name = customCulture.Name;
			languageFile.LanguageInfo.NativeName = customCulture.NativeName;
			languageFile.LanguageInfo.LCID = customCulture.LCID;
			
			// Also add all keys in the rootLanguageFile, if there are any.
			if(rootLanguageFile.Keys != null || rootLanguageFile.Keys.Length > 0)
			{
				Debug.Log("@CreateNewLanguage - ");
				languageFile.Keys = new LanguageKeys[rootLanguageFile.Keys.Length];

				for(int i = 0; i < rootLanguageFile.Keys.Length; i++)
				{
					Debug.Log("@CreateNewLanguage - ");
					languageFile.Keys[i].Key = rootLanguageFile.Keys[i].Key;
				}
			}

			// Add to the language array in the database.
			if(languageFiles == null || languageFiles.Length <= 0)
			{
				Debug.Log("@CreateNewLanguage - ");
				//languageFiles = new LanguageFile[0];
			}

			if(languageFiles != null || languageFiles.Length > 0)
			{
				Debug.Log("@CreateNewLanguage - " + languageFile.LanguageInfo.EnglishName);
				languageFiles.AddItemToArray(languageFile);
			}
		}

		private bool CheckForKey (LanguageKeys key)
		{
			for(int i = 0; i < rootLanguageFile.Keys.Length; i++)
			{
				if(rootLanguageFile.Keys[i].Key == key.Key)
				{
					// Check if the keys list allready contains a key we wanted to add.
					return true;
				}
			}
			
			return false;
		}

		private bool CheckForKey (LanguageFile languageFile, LanguageKeys key)
		{
			for(int i = 0; i < languageFile.Keys.Length; i++)
			{
				if(languageFile.Keys[i].Key == key.Key)
				{
					// Check if the keys list allready contains a key we wanted to add.
					return true;
				}
			}
			
			return false;
		}
	}
}

/*
/// <summary>
		/// Gets all origin keys.
		/// </summary>
		/// <returns>The all origin keys.</returns>
		public List<LanguageKeys> GetAllOriginKeys ()
		{
			List<LanguageKeys> keys = new List<LanguageKeys>();

			if(rootLanguageFile.Keys != null || rootLanguageFile.Keys.Length > 0)
			{
				for(int i = 0; i < rootLanguageFile.Keys.Length; i++)
				{
					keys.Add(rootLanguageFile.Keys[i]);
				}
			}

			return keys;
		}

		/// <summary>
		/// Gets the available languages.
		/// </summary>
		/// <returns>The available languages.</returns>
		public List<LanguageFile> GetAvailableLanguages ()
		{
			List<LanguageFile> availableLanguages = new List<LanguageFile>();

			if(languageFiles != null || languageFiles.Length > 0)
			{
				for(int i = 0; i < languageFiles.Length; i++)
				{
					availableLanguages.Add(languageFiles[i]);
				}
			}

			return availableLanguages;
		}

		/// <summary>
		/// Gets all languages.
		/// </summary>
		/// <returns>The all languages.</returns>
		public List<CustomCultureInfo> GetAllLanguages ()
		{
			List<CustomCultureInfo> customCultureInfos = new List<CustomCultureInfo>();

			if(rootLanguageFile.AllCultureInfos != null || rootLanguageFile.AllCultureInfos.Length > 0)
			{
				for(int i = 0; i < rootLanguageFile.AllCultureInfos.Length; i++)
				{
					customCultureInfos.Add(rootLanguageFile.AllCultureInfos[i]);
				}
			}

			return customCultureInfos;
		}

		/// <summary>
		/// Adds the new language.
		/// </summary>
		/// <param name="language">Language.</param>
		public void AddNewLanguage (LanguageFile language)
		{
			// TODO: Check the performance
			// TODO: Check if we have to change something else, like the rootlanguagefile, or saving it back in the xml.
			bool isAllreadyUsed = false;

			for(int i = 0; i < languageFiles.Length; i++)
			{
				if(languageFiles[i].LanguageInfo.EnglishName.Equals(language.LanguageInfo.EnglishName))
				{
					isAllreadyUsed = true;
				}
			}

			if(!isAllreadyUsed)
			{
				// Language don't exists yet so we can add it.
				languageFiles = languageFiles.AddItemToArray(language);
			}
		}

		/// <summary>
		/// Deletes the language.
		/// </summary>
		/// <param name="language">Language.</param>
		public void DeleteLanguage (LanguageFile language)
		{
			// TODO: Check the performance 
			// TODO: Check if we have to change something else, like the rootlanguagefile, or saving it back in the xml.
			bool hasLanguage = false;
			int index = -1;

			for(int i = 0; i < languageFiles.Length; i++)
			{
				if(languageFiles[i].LanguageInfo.EnglishName.Equals(language.LanguageInfo.EnglishName))
				{
					hasLanguage = true;
					index = i;
				}
			}

			if(hasLanguage)
			{
				// We have the language in our list, so delete it and resize the list.
				languageFiles.RemoveAt(index);
			}
		}

		/// <summary>
		/// Adds a new key.
		/// </summary>
		/// <param name="key">Key.</param>
		public void AddNewKey (LanguageKeys key)
		{
			if(!CheckForKey(rootLanguageFile, key))
			{
				rootLanguageFile.Keys = rootLanguageFile.Keys.AddItemToArray(key);
			}
		}

		/// <summary>
		/// Adds the new key to language.
		/// </summary>
		public void AddNewKeyToLanguage (LanguageFile languageFile,LanguageKeys key)
		{
			for(int i = 0; i < languageFiles.Length; i++)
			{
				if(languageFiles[i].LanguageInfo.EnglishName.Equals(languageFile.LanguageInfo.EnglishName))
				{
					if(!CheckForKey(languageFiles[i], key))
					{
						languageFiles[i].Keys.AddItemToArray(key);
					}
				}
			}
		}

		/// <summary>
		/// Adds the new key to all languages.
		/// </summary>
		/// <param name="key">Key.</param>
		public void AddNewKeyToAllLanguages (LanguageKeys key)
		{
			for(int i = 0; i < languageFiles.Length; i++)
			{
				Debug.Log("Check every languageFile @AddNewKeyToAllLanguages");
				if(!CheckForKey(languageFiles[i], key))
				{
					Debug.Log("In this language: " + languageFiles[i].LanguageInfo.EnglishName + " there is no key: " + key.Key + " so add it.");
					languageFiles[i].Keys.AddItemToArray(key);
				}
			}
		}
		
		public void DeleteKey (LanguageKeys key)
		{
			// TODO: Check the performance
			// TODO: Check if we have to change something else, like the rootlanguagefile, or saving it back in the xml.
			if(CheckForKey(rootLanguageFile, key))
			{
				// Delete the key and resize the array.
				rootLanguageFile.Keys = rootLanguageFile.Keys.RemoveItemFromArray(key);
			}
		}

		/// <summary>
		/// Deletes the key from language.
		/// </summary>
		public void DeleteKeyFromLanguage (LanguageFile languageFile, LanguageKeys key)
		{
			for(int i = 0; i < languageFiles.Length; i++)
			{
				if(languageFiles[i].LanguageInfo.EnglishName.Equals(languageFile.LanguageInfo.EnglishName))
				{
					if(CheckForKey(languageFile, key))
					{
						languageFiles[i].Keys.RemoveItemFromArray(key);
					}
				}
			}
		}

		public void DeleteKeyFromAllLanguages (LanguageKeys key)
		{
			for(int i = 0; i < languageFiles.Length; i++)
			{
				if(CheckForKey(languageFiles[i], key))
				{
					languageFiles[i].Keys.RemoveItemFromArray(key);
				}
			}
		}

		private bool CheckForKey (RootLanguageFile rootFile, LanguageKeys key)
		{	
			for(int i = 0; i < rootLanguageFile.Keys.Length; i++)
			{
				if(rootLanguageFile.Keys[i].Key == key.Key)
				{
					// Check if the keys list allready contains a key we wanted to add.
					return true;
				}
			}

			return false;
		}

		private bool CheckForKey (LanguageFile languageFile, LanguageKeys key)
		{
			for(int i = 0; i < languageFile.Keys.Length; i++)
			{
				if(languageFile.Keys[i].Key == key.Key)
				{
					// Check if the keys list allready contains a key we wanted to add.
					return true;
				}
			}

			return false;
		}
*/