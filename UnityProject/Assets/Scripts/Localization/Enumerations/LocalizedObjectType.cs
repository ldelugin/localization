﻿using UnityEngine;
using System.Collections;

// TODO: Properly telling what each properties and fields are doing.
// TODO: Add more types, like GAMEOBJECT (PREFAB), GUITEXTURE, GUITEXT, NGUITEXT (MAYBE), NEW UNITY GUI SYSTEM, AUDIOCLIP, SPRITE

namespace Sparrow.Localization
{
	/// <summary>
	/// Localized object type.
	/// </summary>
	public enum LocalizedObjectType 
	{
		STRING,
		UNKNOWN,
	}
}